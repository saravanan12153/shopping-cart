class CartItem < ActiveRecord::Base
  belongs_to :cart, counter_cache: :items_count
  belongs_to :product

  validates_presence_of :cart, :product
  validates_numericality_of :quantity, greater_than: 0, only_integer: true

  delegate :name, :price, to: :product

  def total
    quantity * price
  end
end
