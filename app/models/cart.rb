class Cart < ActiveRecord::Base
  has_many :items, class_name: 'CartItem'

  def total
    if items.any?
      items.map(&:total).reduce(:+)
    else
      0
    end
  end
end
