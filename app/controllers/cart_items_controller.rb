class CartItemsController < ApplicationController
  def add 
    item = shopping_cart.items.find_or_initialize_by(product_id: params[:product_id])
    if item.new_record?
      item.quantity = 1
      item.save
    else
      item.increment!(:quantity)
    end
  end

  def destroy
    item = shopping_cart.items.find(params[:id])
    item.destroy
    @items = shopping_cart.items
    @total = shopping_cart.total
  end
end
