require 'rails_helper'

RSpec.describe 'homepage', type: :routing do
  it 'routes / to products#index' do
    expect(get: root_path).to route_to(controller: 'products', action: 'index')
  end
end
