require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe '#index' do
    context 'when has no products' do
      it 'renders template without products' do
        get :index
        expect(assigns(:products)).to be_empty
        expect(response).to render_template :index
      end
    end

    context 'when has products' do
      it 'renders template with products' do
        some  = Product.create!(name: 'some book', price: 50)
        other = Product.create!(name: 'other book', price: 35)
        get :index
        expect(assigns(:products)).to eq [other, some]
        expect(response).to render_template :index
      end
    end
  end
end
