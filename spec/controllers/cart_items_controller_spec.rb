require 'rails_helper'

RSpec.describe CartItemsController, type: :controller do
  describe '#add' do
    let!(:cart) { Cart.create! }
    let!(:product) { Product.create!(name: 'book', price: 50) }
    before { session[:cart_id] = cart.id }

    context 'when item does not exist' do
      it 'creates an item for shopping cart' do
        xhr :patch, :add, product_id: product
        expect(cart.reload.items_count).to eq 1
        expect(response).to render_template :add
      end
    end

    context 'when item already exists' do
      it 'increments the item quantity' do
        item = CartItem.create!(cart: cart, product: product, quantity: 1)
        xhr :patch, :add, product_id: product
        expect(cart.reload.items_count).to eq 1
        expect(item.reload.quantity).to eq 2
        expect(response).to render_template :add
      end
    end
  end

  describe '#destroy' do
    it 'destroys an item of shopping cart' do
      cart              = Cart.create!
      session[:cart_id] = cart.id
      product           = Product.create!(name: 'book', price: 50)
      item              = CartItem.create!(cart: cart, product: product, quantity: 1)
      xhr :delete, :destroy, id: item
      cart.reload
      expect(cart.items_count).to be_zero
      expect(cart.items).to be_empty
      expect(assigns(:items)).to be_empty
      expect(assigns(:total)).to eq 0
      expect(response).to render_template :destroy
    end
  end
end
