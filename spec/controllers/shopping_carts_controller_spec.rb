require 'rails_helper'

RSpec.describe ShoppingCartsController, type: :controller do
  describe '#show' do
    let(:shopping_cart) { Cart.create }
    before { session[:cart_id] = shopping_cart.id }

    context 'when has no items' do
      it 'renders shopping cart without items' do
        get :show
        expect(assigns(:items)).to be_empty
        expect(assigns(:total)).to eq 0
        expect(response).to render_template :show
      end
    end

    context 'when has items' do
      it 'renders shopping cart with items' do
        product = Product.create!(name: 'book', price: 50)
        item = CartItem.create!(cart: shopping_cart, product: product, quantity: 1)
        get :show
        expect(assigns(:items)).to eq [item]
        expect(assigns(:total)).to eq 50
        expect(response).to render_template :show
      end
    end
  end
end
