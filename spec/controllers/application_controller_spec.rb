require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  controller do
    def index
      render nothing: true
    end
  end

  describe '#shopping_cart' do
    context 'when shopping cart does not exist' do
      it 'creates a new shopping cart' do
        get :index
        expect(controller.shopping_cart).to be_persisted
        expect(session[:cart_id]).not_to be_blank
      end
    end

    context 'when shopping cart already exists' do
      it 'returns the shopping cart existent' do
        cart = Cart.create
        session[:cart_id] = cart.id
        get :index
        expect(session[:cart_id]).to eq cart.id
        expect(controller.shopping_cart).to eq cart
      end
    end
  end
end
