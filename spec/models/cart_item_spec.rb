require 'rails_helper'

RSpec.describe CartItem, type: :model do
  subject(:item) { CartItem.new }

  context 'relations' do
    it { expect(item).to belong_to(:cart).counter_cache(:items_count) }
    it { expect(item).to belong_to(:product) }
  end

  context 'validations' do
    it { expect(item).to validate_presence_of(:cart) }
    it { expect(item).to validate_presence_of(:product) }
    it { expect(item).to validate_numericality_of(:quantity).is_greater_than(0).only_integer }
  end

  context 'delegates' do
    it { expect(item).to respond_to(:name) }
    it { expect(item).to respond_to(:price) }
  end

  describe '#total' do
    it 'multiplies quantity by price' do
      product = Product.new(price: 50)
      item    = CartItem.new(quantity: 2, product: product)
      expect(item.total).to eq 100
    end
  end
end
