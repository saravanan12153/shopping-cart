require 'rails_helper'

RSpec.describe Cart, type: :model do
  subject(:cart) { Cart.new }

  context 'relations' do
    it { expect(cart).to have_many(:items).class_name('CartItem') }
  end

  context '#total' do
    context 'when has no items' do
      it 'should be zero' do
        cart = Cart.new
        expect(cart.total).to eq 0
      end
    end

    context 'when has some items' do
      it 'sum total of the items' do
        product       = Product.new(price: 50)
        other_product = Product.new(price: 20)
        item          = CartItem.new(product: product, quantity: 1)
        other_item    = CartItem.new(product: other_product, quantity: 2)
        cart          = Cart.new(items: [item, other_item])
        expect(cart.total).to eq 90
      end
    end
  end
end
