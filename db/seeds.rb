# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Product.create([
  { name: 'Metaprogramming Ruby 2: Program Like the Ruby Pros', price: 48 },
  { name: 'Master the Art of Handling Failure in Ruby', price: 15 },
  { name: 'Build Awesome Command-Line Applications in Ruby', price: 39 },
  { name: 'Agile Web Development with Rails 4', price: 56 }
])
