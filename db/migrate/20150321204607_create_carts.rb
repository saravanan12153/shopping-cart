class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.integer :items_count, null: false, default: 0
      t.timestamps null: false
    end
  end
end
