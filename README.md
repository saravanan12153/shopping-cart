# Carrinho de compras

[https://carrinho-de-compras.herokuapp.com/](https://carrinho-de-compras.herokuapp.com/)

## Executando localmente

O projeto usa SQLite em ambiente de desenvolvimento, então basta executar:

    $ rake db:migrate
    $ rake db:seed
    $ rails server
