Rails.application.routes.draw do
  root 'products#index'
  resource :shopping_cart, only: [:show]
  resources :cart_items, only: [:destroy] do
    patch :add, on: :collection
  end
end
